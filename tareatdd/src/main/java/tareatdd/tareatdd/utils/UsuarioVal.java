package tareatdd.tareatdd.utils;

public class UsuarioVal {

    public boolean validarNombre(String nombre){
        boolean Correcto = true;
        for (int i = 0; i < nombre.length(); i++) {
            if(Character.isDigit(nombre.charAt(i))){
                Correcto = false;
            }

        }
        return Correcto;
    }

    public boolean validarRut(String run){
        boolean validacion = false;
        try {
            run =  run.toUpperCase();
            run = run.replace(".", "");
            run = run.replace("-", "");
            int rutAux = Integer.parseInt(run.substring(0, run.length() - 1));

            char dv = run.charAt(run.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (Exception ignored) {

        }
        return validacion;

    }

    public boolean validarNumero(String nro){

            String patron = "\\d{1,9}";

            if (nro.matches(patron)) {
                return true;
            } else {
                return false;
            }
        }

    public static boolean validarEdad(int edad) {
        if (edad > 0 && edad >= 1 && edad <= 100) {
            return true;
        } else {
            return false;
        }
    }


}
