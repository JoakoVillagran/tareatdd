package tareatdd.tareatdd.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tareatdd.tareatdd.utils.Usuario;
import tareatdd.tareatdd.utils.UsuarioVal;

@RestController
public class usuarioController {

    @PostMapping("/crear-usuario")
    public ResponseEntity<String> crearUsuario(@RequestBody Usuario usuario) {
        StringBuilder mensaje = new StringBuilder();
        boolean usuarioValido = true;
        UsuarioVal validacion = new UsuarioVal();

        if (!validacion.validarNombre(usuario.getNombre())) {
            mensaje.append("El nombre es incorrecto. ");
            usuarioValido = false;
        }

        if (!validacion.validarNombre(usuario.getApellidoPaterno())) {
            mensaje.append("El apellido paterno es incorrecto. ");
            usuarioValido = false;
        }

        if (!validacion.validarNombre(usuario.getApellidoMaterno())) {
            mensaje.append("El apellido materno es incorrecto. ");
            usuarioValido = false;
        }

        if (!validacion.validarRut(usuario.getRut())) {
            mensaje.append("El Rut es incorrecto. ");
            usuarioValido = false;
        }

        if (!validacion.validarNumero(usuario.getNumeroTelefonico())) {
            mensaje.append("El número telefónico es incorrecto. ");
            usuarioValido = false;
        }

        if (!validacion.validarEdad(usuario.getEdad())) {
            mensaje.append("La edad es incorrecta. ");
            usuarioValido = false;
        }

        if (usuarioValido) {
            return ResponseEntity.ok("El usuario es válido.");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mensaje.toString());
        }

    }
}
